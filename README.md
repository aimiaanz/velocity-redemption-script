											/************************************************************/
														--CMPGN CODE: ANZCTRD062
														--CMPGN TYPE: Partner
														--CMPGN NAME: Velocity November17 Redemption		
														--DATE ADDED: 20th October 2017
														--CREATED BY: Prasad(WNS) and Mariya Duarte
														--Updated By: Prasad on 1st November 2017 
											/************************************************************/


--Received a Campaign last week - Need to exclude them
drop table if exists sbx.ANZCTRD062_rec_cmp_wc171023;
create table sbx.ANZCTRD062_rec_cmp_wc171023 as 
select distinct b.mem_id
from  dwh.cmpgn_interac_fct a left join dwh.mem b on a.mem_key = b.mem_key
left join dwh.cmpgn_dim c on a.cmpgn_dim_key = c.cmpgn_dim_key
where a.interac_type_dim_key in ('17028') and interac_dt_key>='2017-10-23' ;

select count(*) from sbx.ANZCTRD062_rec_cmp_wc171023; --87063

grant all on sbx.ANZCTRD062_rec_cmp_wc171023 to cmx_role;

--Received Onboarding Previously
drop table if exists sbx.ANZCTRD062_Received_OB3;
create table sbx.ANZCTRD062_Received_OB3 as
select distinct b.mem_id
from  dwh.cmpgn_interac_fct a left join dwh.mem b on a.mem_key = b.mem_key
left join dwh.cmpgn_dim c on a.cmpgn_dim_key = c.cmpgn_dim_key
where a.interac_type_dim_key in ('17028')
and c.cmpgn_proj_id in ('ANZCJOB003');

select count(*) from sbx.ANZCTRD062_Received_OB3; --19249

grant all on sbx.ANZCTRD062_Received_OB3 to cmx_role;


DROP table if exists sbx.ANZCTRD062_Initial_Flags;

create table sbx.ANZCTRD062_Initial_Flags as
select distinct
       a.mem_key, 
	   a.mem_id, 
	   a.std_mem_stat_nm,
	   a.pgm_id,
	   a.acct_creat_dt,
	   case when a.acct_creat_dt<=TIMESTAMPADD(month, -3, current_timestamp) then 1 else 0 end as Open_90days_prior,--Opened their account before 90 days
	   a.acct_activ_dt,
	   a.mem_ext_char_3 as eDM_flag,
	   a.mem_ext_dt_2 as First_Pur_dt,
	   a.mem_ext_dec_1 as CCM1_Optout_flag,
	   a.mem_ext_dec_4 as eStatement_flag,
	   a.mem_ext_dec_5 as Acct_act_online_flag,
	   b.consent_lvl,
	   case when b.consent_lvl in ('BASE_LV3','TS-LV3') then 1 else 0 end as consent_lvl3,--Level3 Consent
	   b.CCM1_emailoptin_dt,
	   b.CCM1_emailoptout_dt,
	   b.CCM2_suppressed_dt,
	   b.edm_Optin_dt,
	   b.eStatements_Optin_dt,
	   w.email_to_Use,
	   ad.pts_bal,
       case when ad.pts_bal >='0' then 1 else 0 end as pts_bal_grt_0,--Points balance greater than zero
       case when a.mem_id in (select mem_id from sbx.ch_summerised_live where tot_hard_bounce >= '1') then 1 else 0 end as hardbounce,--Hard Bounced more than once
	   case when a.mem_id in (select mem_id from sbx.ch_summerised_live where supressed_email >= '1') then 1 else 0 end as suppressed,--Email suppressed more than once
       case when a.mem_id in (select mem_id from sbx.ch_summerised_live where tot_soft_bounce >= '3') then 1 else 0 end as soft_bounce,--Soft bounce more than three times
       case when a.mem_id in (select mem_id from sbx.ch_summerised_live where sp_optout >= '1') then 1 else 0 end as sp_optout,--Opted out through Silverpop
       0 as ANZCTRD062_ch,/* This is the first batch */
       case when d.mem_id is not null then 1 else 0 end as Received_OB3_Prev,--Target collectors who have received atleast one of the OB's 
       case when e.mem_id is not null then 1 else 0 end as Receivd_cmp_last_week, --Excluidng collectors who received a campaign last week    
       c.activ_dt_key,
       c.PPEmail_CCM2_Match,
       c.ANZEmail_CCM2_Match,
       c.CCMEmail_CCM2_Match,
       c.CCM1_match,
       case when a.mem_id in ('AZ30757578','AZ307648282','AZ00045048','BC71544309','AZ30505728','AZ30003998','AZ30289230','AZ00005054','AZ00047130','BC70027428','AZ30616061') then 1 else 0 end as Black_List
from cmx.mem_dim a 
left join pii.acctdetails ad on a.mem_id=ad.rewards_id
left join sbx.lcmi_consent_outcomes b on a.mem_id = b.mem_id 
left join pii.customer_contacts c on a.mem_id = c.mem_id 
left join sbx.Received_Onboarding d on a.mem_id= d.mem_id
left join sbx.received_cmp_last_week e on a.mem_id =e.mem_id
left join sbx.email_priority_post_wash w on a.mem_id=w.mem_id
;

select count(*), count(distinct mem_id) from sbx.ANZCTRD062_Initial_Flags; --608570	608570

grant all on sbx.ANZCTRD062_Initial_Flags to cmx_role;


--Final Selections table with all exclusions
drop table if exists sbx.ANZCTRD062_selections_final ;

create table sbx.ANZCTRD062_selections_final as
select a.*, a.email_to_use as email, 
       cast(to_char(sysdate,'YYYYMMDD') as int) as datestamp
       --select count(*), count(distinct mem_id)
from sbx.ANZCTRD062_Initial_Flags a   --608570	608570
where  a.std_mem_stat_nm = 'Account in order' -- Get active accounts --432365	432365
and    a.pgm_id = 'ANZRW' -- Get consumer (ARW) only --279314	279314
and    a.activ_dt_key is not null and a.activ_dt_key<>'0001-01-01'--Activated their account --253142	253142
and    a.CCM1_match = 'Y' -- Find those who have matched EDM CCM1 File --162658	162658
and    a.hardbounce = '0' --160167	160167
and    a.suppressed = '0' --157739	157739
and    a.pts_bal_grt_0='1'  --157647	157647
and    a.consent_lvl3 = '1' --54403	54403
and    a.soft_bounce = '0' --54048	54048
and    a.sp_optout = '0' --53889	53889
AND   (A.Open_90days_prior='1' OR A.Received_OB3_Prev = '1') --52974	52974
--and    a.Receivd_cmp_last_week = '0' --40223	40223    --MD: removed this constrain as already more then a week away
AND    a.email_to_use <>'REVIEW' AND a.email_to_use IS NOT NULL --52456	52456
and    a.Black_List=0 --52456	52456
and    a.mem_id not in (select mem_id from sbx.no_contact_90 where contact_90days >= '10') --52456	52456
and    a.mem_id not in (select mem_id from sbx.no_contact_28 where contact_28days >= '5') --52456	52456
and    a.mem_id not in (select mem_id from sbx.no_contact_7 where contact_7days >= '2') --52456	52456
;

Grant all on sbx.ANZCTRD062_selections_final to cmx_role;

select *
from public.ANZCTRD062_Audience

/*
select count(*),COUNT(distinct mem_id),CCM1_emailoptout_dt
from sbx.ANZCTRD062_selections_final
group by CCM1_emailoptout_dt;
--22848	22847	


--One Customer duplicate entires, will be removed in Unica
select count(*), mem_id
from sbx.ANZCTRD062_selections_final
group by mem_id
having count(*)>1;
--2	AZ30295534


select b.consent_lvl,a.CCM1_emailoptout_dt
,count(*),COUNT(distinct a.mem_id),latest_ccm2_flag  from  sbx.ANZCTRD062_selections_final a
left join sbx.lcmi_consent_outcomes b on a.mem_id=b.mem_id
group by b.consent_lvl,latest_ccm2_flag,a.CCM1_emailoptout_dt;
/*
consent_lvl	CCM1_emailoptout_dt	count	COUNT	latest_ccm2_flag
BASE_LV3						22848	22847	N                                   
*/

select * 
from  sbx.ANZCTRD062_selections_final a
left join sbx.lcmi_consent_outcomes b on a.mem_id=b.mem_id
where b.CCM1_emailoptout_dt is not null
and b.consent_lvl='TS-LV3';
--Null

